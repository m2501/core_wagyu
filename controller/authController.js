"use-strict";

const md5 = require("md5");

const register = async (req, res) => {
  // validation rules
  req.rules = {
    firstname: req.joi.string().required(),
    lastname: req.joi.string().required(),
    email: req.joi
      .string()
      .email()
      .required(),
    phone: req.joi.string().required(),
    password: req.joi.string().required()
  };

  let body = await req.validation(req, res);
  if (!body.valid) {
    return req.output(
      req,
      res,
      { error: true, message: body[0].message },
      "error",
      400
    );
  }

  req.body.username = (req.body.firstname + req.body.lastname).toLowerCase();
  req.body.password = md5(req.body.password);

  try {
    // check email
    let checkEmail = await req.services.userService.detail(req, {
      where: {
        email: req.body.email
      }
    });

    if (checkEmail) {
      return req.output(
        req,
        res,
        { error: true, message: "email already exists" },
        "error",
        500
      );
    }

    // check phone
    let checkPhone = await req.services.userService.detail(req, {
      where: {
        phone: req.body.phone
      }
    });

    if (checkPhone) {
      return req.output(
        req,
        res,
        { error: true, message: "phone already exists" },
        "error",
        500
      );
    }

    await req.services.userService.create(req, req.body);

    let objectResponse = await {
      error: false,
      message: "success",
      data: {}
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    return req.output(req, res, { error: true, message: err }, "error", 500);
  }
};

const login = async (req, res) => {
  // validation rules
  req.rules = {
    email: req.joi
      .string()
      .email()
      .required(),
    password: req.joi.string().required()
  };

  let body = await req.validation(req, res);
  if (!body.valid) {
    return req.output(
      req,
      res,
      { error: true, message: body[0].message },
      "error",
      400
    );
  }

  try {
    let check = await req.services.userService.detail(req, {
      where: {
        email: req.body.email,
        password: md5(req.body.password)
      }
    });

    if (!check) {
      return req.output(
        req,
        res,
        { error: true, message: "username or password is wrong" },
        "error",
        500
      );
    }

    // generate token
    let token = req.jwt.generatePublicKey(req.body);

    let objectResponse = await {
      error: false,
      message: "success",
      data: {
        token: token
      }
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    return req.output(req, res, { error: true, message: err }, "error", 500);
  }
};

module.exports = { register, login };
