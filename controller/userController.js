const detailUser = async (req, res) => {
  try {
    let data = await req.services.userService.detail(req, {
      where: {
        id: req.params.id
      },
      attributes: {
        exclude: [ 'password' ]
      }
    });

    let objectResponse = await {
      error: false,
      message: data === null ? "not found" : "found",
      data: data
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    return req.output(req, res, { error: true, message: err }, "error", 500);
  }
};

module.exports = { detailUser };
