"use-strict";

const create = async (req, params) => {
  let data = await req.models.users.create(params);
  return data;
};

const detail = async (req, params) => {
  let data = await req.models.users.findOne(params);
  return data;
};

module.exports = { create, detail };
