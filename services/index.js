"use strict";

const fs = require("fs");
const path = require("path");
const basename = path.basename(__filename);
const service = {};

fs.readdirSync(__dirname)
  .filter(file => {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach(file => {
    const fileJoin = path.join(__dirname, file);
    const name = file.split(".");
    service[name[0]] = require(fileJoin);
  });

Object.keys(service).forEach(name => {
  if (service[name].associate) {
    service[name].associate(service);
  }
});

module.exports = service;
