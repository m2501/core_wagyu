"use strict";

module.exports = async (req, res) => {
  let bodyRules = req.joi.object(req.rules);

  if (Object.keys(req.body).length === 0) {
    return req.output(
      req,
      res,
      { error: true, message: "body not found" },
      "error",
      500
    );
  }

  try {
    req.body = await bodyRules.validateAsync(req.body);

    return {
      body: req.body,
      valid: true
    };
  } catch (err) {
    return err.details;
  }
};
