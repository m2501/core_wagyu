"use strict";

const jwt = require("jsonwebtoken");
const fs = require("fs");
const { generateKeyPairSync } = require("crypto");
const optionFs = { recursive: true };

const authenticateToken = (req, res, next) => {
  const privateKey = fs.readFileSync(
    __dirname + "/../storage/private.key",
    "utf8"
  );

  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  if (token == null)
    return req.output(
      req,
      res,
      { error: true, message: "Unauthorized" },
      "error",
      403
    );

  jwt.verify(token, privateKey, (err, user) => {
    console.log(err);
    console.log(user);
    if (err)
      return req.output(
        req,
        res,
        { error: true, message: "Unauthentication" },
        "error",
        403
      );

    next();
  });
};

const generatePublicKey = payload => {
  const privateKey = fs.readFileSync(
    __dirname + "/../storage/private.key",
    "utf8"
  );

  return jwt.sign({ data: payload }, privateKey, {
    expiresIn: "24h"
  });
};

const generatePrivateKey = (req, res) => {
  const { privateKey } = generateKeyPairSync("rsa", {
    modulusLength: 1028,
    privateKeyEncoding: {
      type: "pkcs1",
      format: "pem"
    }
  });

  let dir = __dirname + "/../storage";

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, optionFs);
  }

  fs.appendFileSync(dir + "/private.key", privateKey);

  return req.output(
    req,
    res,
    { error: false, message: "success generate private key" },
    "info",
    200
  );
};

module.exports = {
  authenticateToken,
  generatePublicKey,
  generatePrivateKey
};
