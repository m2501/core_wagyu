"use-strict";

const express = require("express");
const router = express.Router();
const jwt = require("../utils/jwt");
const middleware = jwt.authenticateToken;
const authController = require("../controller/authController");
const userController = require("../controller/userController");

module.exports = app => {
  // utils
  router.get("/generate", jwt.generatePrivateKey);

  // auth
  router.post("/register", authController.register);
  router.post("/login", authController.login);

  // user
  router.post("/users", middleware, userController.detailUser);
  router.get("/user/detail/:id", middleware, userController.detailUser);

  app.use("/api/v1", router);
};
